<?php

declare(strict_types=1);

$resource = new Site\Core\Resource(
    path: new Fire\Path\JoinManifest(new Fire\Path\Join(PUBLIC_DIR), MANIFEST_FILE),
    url: new Fire\Path\JoinManifest(new Fire\Path\Join(Site\Core\devServer(WP_HOME)), MANIFEST_FILE),
);

(new Emrl\Admin\LiveChat())->register();
(new Fire\Core\CacheBustScripts())->register();
(new Fire\Menu\Descriptions())->register();
(new Fire\Post\Type\Hooks())->register();
(new Fire\Term\Taxonomy\Hooks())->register();
(new Site\Admin\Admin($resource))->register();
(new Site\Admin\Login($resource))->register();
(new Site\Admin\SiteOptions())->register();
(new Site\Block\Block())->register();
(new Site\Core\GoogleApiKey(GOOGLE_API_KEY))->register();
(new Site\Core\Headers())->register();
(new Site\Core\Mail())->register();
(new Site\Core\NavMenu())->register();
(new Site\Core\Request())->register();
(new Site\Core\HashedUploadDirectory())->register();
(new Site\Editor\Editor($resource))->register();
(new Site\Integration\AdvancedCustomFields\AdvancedCustomFields())->register();
(new Site\Integration\GravityForms\GravityForms($resource))->register();
(new Site\Integration\TheEventsCalendar\TheEventsCalendar())->register();
(new Site\Page\Page())->register();
(new Site\Post\Blocks\Post\Block())->register();
(new Site\Post\Blocks\Post\BlockFieldGroup())->register();
(new Site\Post\Post())->register();
(new Site\Theme\ArchiveTitle())->register();
(new Site\Theme\Blocks\Card\CardBlock())->register();
(new Site\Theme\Blocks\CardBody\CardBodyBlock())->register();
(new Site\Theme\Blocks\CardFooter\CardFooterBlock())->register();
(new Site\Theme\Blocks\CardHeader\CardHeaderBlock())->register();
(new Site\Theme\Blocks\DetailsSummary\Block())->register();
(new Site\Theme\Shortcodes($resource))->register();
(new Site\Theme\Theme($resource))->register();

(new Fire\Admin\RemoveDashboardWidgets(
    'dashboard_primary',
    'dashboard_quick_press',
    'wpseo-dashboard-overview',
    'wordfence_activity_report_widget',
    'tribe_dashboard_widget',
))->register();
