<?php

namespace Deployer;

set_include_path('./vendor'.PATH_SEPARATOR.$_SERVER['HOME'].'/.composer/vendor'.PATH_SEPARATOR.get_include_path());
require_once 'recipe/common.php';
require_once 'contrib/php-fpm.php';
require_once 'emrl/deployer-emrl/recipe/common.php';
require_once 'emrl/deployer-emrl/recipe/opalstack.php';
require_once 'emrl/deployer-emrl/recipe/wordpress.php';

// Configuration
set('output_path', [
    'public',
    'src',
    'theme',
    'vendor',
    'app.php',
    'config.php',
]);

set('shared_dirs', [
    'public/c/uploads',
    'public/c/wflogs',
]);

set('shared_files', [
    '.env',
    'wp-cli.yml',
    'public/.user.ini',
]);

set('rsync_config', [
    'flags' => '',
    'options' => [
        '--recursive',
        '--links',
        '--compress',
        '--verbose',
        '--delete',
        '--executability',
        '--chmod=ugo=rwX',
        // WordPress drop-ins
        '--exclude=/public/c/*.php',
        // Wordfence
        '--exclude=/public/w/wordfence-waf.php',
        // BackWPup logs (root relative so it applies to media and not the plugin)
        '--exclude=/backwpup*',
    ],
]);

set('uploads_path', 'public/c/uploads');

// Hosts
localhost()
    ->set('url', $_SERVER['APP_URL'] ?? 'http://localhost')
    ->set('database_backup_path', '/tmp');

host('staging')
    ->setHostname('emrl.dev')
    ->setRemoteUser('deploy')
    ->setDeployPath('~/webapps/APP')
    ->setLabels(['stage' => 'staging'])
    ->set('keep_releases', 2)
    ->set('url', 'https://APP.emrl.dev')
    ->set('bin/php', '/usr/bin/php83')
    ->set('bin/wp', '{{bin/php}} ~/bin/wp')
    ->set('php_fpm_service', 'php83-php-fpm.service');

host('production')
    ->setHostname('opalX.opalstack.com')
    ->setRemoteUser('')
    ->setDeployPath('~/apps/APP')
    ->setLabels(['stage' => 'production'])
    ->set('url', 'https://APP.com')
    ->set('bin/php', '/usr/bin/php83')
    ->set('bin/wp', '{{bin/php}} ~/bin/wp');

// Tasks
task('php-fpm:reload')->select('stage=staging');
task('service:restart')->select('stage=production');

task('deploy', [
    'deploy:setup',
    'deploy:lock',
    'deploy:release',
    'deploy:warm_release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:clear_paths',
    'deploy:symlink',
    'theme:roots',
    'php-fpm:reload',
    'service:restart',
    'deploy:unlock',
    'deploy:cleanup',
    'deploy:success',
])
    ->desc('Deploy application');

after('deploy:failed', 'deploy:unlock');
after('rollback', 'theme:roots');
after('rollback', 'php-fpm:reload');
after('rollback', 'service:restart');
