<?php
/**
 * Plugin Name: WP-CLI Commands
 * Description: Register custom commands
 * Author: EMRL
 * Author URI: http://emrl.com
 * Version: 1.0.0
 */

if (class_exists(WP_CLI::class)) {
    (new Emrl\Console\ResetThemeRootsCommand())->register();
}
