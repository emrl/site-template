<?php

// Ignore 8.1 deprecation notices from WordPress and plugins
if (WP_DEBUG) {
    set_error_handler(function (int $errno, string $errstr, string $errfile): bool {
        if (
            str_starts_with($errfile, ABSPATH)
            || str_starts_with($errfile, WP_PLUGIN_DIR)
        ) {
            return true;
        }

        return false;
    }, E_DEPRECATED);
}
