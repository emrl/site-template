<?php
/**
 * Plugin Name: Register Theme Directory
 * Description: Register the default Wordpress theme directory
 * Author: EMRL
 * Author URI: http://emrl.com
 * Version: 1.0.0
 *
 * This file must be loaded before any calls to `get_stylesheet_directory()`,
 * thus why its name starts with a dot.
 */

if (!defined('WP_DEFAULT_THEME')) {
    register_theme_directory(ROOT_DIR);

    // @see https://core.trac.wordpress.org/ticket/59847
    register_theme_directory(get_theme_root());
}
