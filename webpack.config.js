import compose from 'lodash/fp/compose.js'
import { dirname } from 'path'
import { fileURLToPath } from 'url'
import alias from './config/webpack/alias.js'
import devServer from './config/webpack/devServer.js'
import entries from './config/webpack/entries.js'
import { filenameBuilder } from './config/webpack/filename.js'
import optimization from './config/webpack/optimization.js'
import output from './config/webpack/output.js'
import rootPathBuilder from './config/webpack/path.js'
import plugins from './config/webpack/plugins.js'
import resources from './config/webpack/resources.js'
import scripts from './config/webpack/scripts.js'
import styles from './config/webpack/styles.js'
import svg from './config/webpack/svg.js'

export default ({
    production = false,
} = {}) => {
    const filename = filenameBuilder(production)
    const rootPath = rootPathBuilder(dirname(fileURLToPath(import.meta.url)))

    return compose(
        ...alias(rootPath),
        devServer(rootPath),
        entries,
        optimization,
        output(rootPath, filename, production),
        ...plugins(filename, production),
        resources,
        scripts,
        ...styles(filename),
        svg,
    )({
        mode: production ? 'production' : 'development',
        externals: {
            // jQuery is included by WordPress
            jquery: 'jQuery',
        },
    })
}
