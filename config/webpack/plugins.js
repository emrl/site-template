import WebpackCssClassExtractPlugin from '@emrl/webpack-css-class-extract-plugin'
import DependencyExtractionWebpackPlugin from '@wordpress/dependency-extraction-webpack-plugin'
import CopyWebpackPlugin from 'copy-webpack-plugin'
import { optimize } from 'svgo'
import webpack from 'webpack'
import { WebpackManifestPlugin } from 'webpack-manifest-plugin'
import { plugin } from 'webpack-partial'
import { OUTPUT_URL } from './constants.js'
import { ext, path } from './filename.js'
import { trailingSlash } from './path.js'
import svgoConfig from './svgo.config.js'

export default (filename, production) => [
    plugin(new CopyWebpackPlugin({
        patterns: [
            {
                from: './resources/images',
                to: filename(path()),
                // Optimize SVGs
                transform(content, path) {
                    if (/\.svg$/.test(path)) {
                        return optimize(
                            content,
                            {
                                path,
                                ...svgoConfig(),
                            },
                        ).data
                    }

                    return content
                },
            },
            {
                from: './resources/static',
                to: filename(path()),
            },
            {
                from: './resources/icons',
                to: filename(path()),
                transform: (content, path) => optimize(
                    content,
                    {
                        path,
                        ...svgoConfig({
                            inline: true,
                        }),
                    },
                ).data,
            },
        ],
    })),
    plugin(new DependencyExtractionWebpackPlugin({
        // https://github.com/WordPress/gutenberg/issues/44674
        outputFilename: filename(ext('.php')),
    })),
    plugin(new WebpackCssClassExtractPlugin()),
    // "eval" based source maps do not work for CSS, this fixes that.
    !production && plugin(new webpack.SourceMapDevToolPlugin({
        test: /\.css$/,
        filename: null,
        append: '/*# sourceMappingURL=[url] */',
    })),
    production && plugin(new WebpackManifestPlugin({
        basePath: trailingSlash(OUTPUT_URL),
        publicPath: trailingSlash(OUTPUT_URL),
    })),
].filter(Boolean)
