const generateName = (
    production = false,
    {
        path = '',
        ext = '[ext]',
        hash = '.[contenthash:10]',
    } = {},
) => `${path}[name]${production ? hash : ''}${ext}`

export const ext = (ext = '[ext]') => ({ ext })
export const path = (path = '[path]') => ({ path })
export const hash = (hash = '.[contenthash:10]') => ({ hash })
export const filenameBuilder = (production = false) => (...args) => generateName(production, Object.assign({}, ...args))
