import { join } from 'path'
import { unslash } from './path.js'

export const OUTPUT_URL = 'c/dist'
export const OUTPUT_PATH = join('./public', OUTPUT_URL)
export const APP_URL = unslash(process.env.APP_URL || 'http://localhost')
export const DEV_SERVER_PORT = process.env.WEBPACK_DEV_SERVER_BASE_PORT || 8080
export const DEV_SERVER_URL = `${APP_URL}:${DEV_SERVER_PORT}`
