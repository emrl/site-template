import { output } from 'webpack-partial'
import { DEV_SERVER_URL, OUTPUT_PATH, OUTPUT_URL } from './constants.js'
import { ext } from './filename.js'
import { slash } from './path.js'

export default (rootPath, filename, production) => output({
    path: rootPath(OUTPUT_PATH),
    publicPath: `${!production ? DEV_SERVER_URL : ''}${slash(OUTPUT_URL)}`,
    filename: filename(ext('.js')),
    assetModuleFilename: filename(ext('[ext][query]')),
    // https://github.com/webpack/webpack-dev-middleware/issues/861
    clean: true,
})
