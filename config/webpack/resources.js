import { loader } from 'webpack-partial'

export default loader({
    test: /\.(woff2?|png|jpe?g|gif)$/,
    type: 'asset',
})
