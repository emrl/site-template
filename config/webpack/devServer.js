import { mkdir, writeFile } from 'fs/promises'
import curry from 'lodash/curry.js'
import { APP_URL, DEV_SERVER_PORT, DEV_SERVER_URL, OUTPUT_PATH } from './constants.js'

export default curry((rootPath, config) => ({
    ...config,
    devServer: {
        host: '0.0.0.0',
        port: DEV_SERVER_PORT,
        static: false,
        allowedHosts: [
            new URL(APP_URL).hostname,
        ],
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        devMiddleware: {
            // Write runtime, SVGs, JSON, and dependency files to disk so we can inline
            writeToDisk: (f) => /(?:runtime(?:~[^.]+)?.js|\.svg|\.php|\.json)$/.test(f),
        },
        server: {
            type: 'https',
            options: {
                cert: './server.crt',
                key: './server.key',
            },
        },
        async onListening() {
            // Helper to let WordPress know where assets are
            await mkdir(rootPath(OUTPUT_PATH), { recursive: true })
            await writeFile(rootPath(OUTPUT_PATH, 'hot'), DEV_SERVER_URL)
        },
    },
}))
