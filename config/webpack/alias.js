import { alias } from 'webpack-partial'

export default (rootPath) => [
    alias('@', rootPath()),
]
