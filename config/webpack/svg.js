import { loader } from 'webpack-partial'
import svgoConfig from './svgo.config.js'

export default loader({
    test: /\.svg(\?.*)?$/,
    oneOf: [
        // Inline
        {
            resourceQuery: /inline/,
            type: 'asset/source',
            use: [
                {
                    loader: 'svg-transform-loader',
                    options: {
                        transformQuery(query) {
                            delete query.inline
                        },
                    },
                },
                {
                    loader: 'svgo-loader',
                    options: {
                        configFile: false,
                        ...svgoConfig({
                            inline: true,
                        }),
                    },
                },
            ],
        },
        // Resource
        {
            type: 'asset',
            use: [
                'svg-transform-loader',
                {
                    loader: 'svgo-loader',
                    options: {
                        configFile: false,
                        ...svgoConfig(),
                    },
                },
            ],
        },
    ],
})
