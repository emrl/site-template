import { entry } from 'webpack-partial'

export default entry({
    main: './resources/scripts/index.js',
    editor: './src/Editor/resources/scripts/index.js',
})
