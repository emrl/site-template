import { loader } from 'webpack-partial'

export default loader({
    test: /\.js$/,
    exclude: /node_modules\/(?!balance-text)/,
    use: [
        {
            loader: 'babel-loader',
            options: {
                presets: [
                    [
                        '@babel/preset-env',
                        {
                            targets: 'defaults',
                        },
                    ],
                ],
                plugins: [
                    '@babel/plugin-transform-react-jsx',
                ],
            },
        },
    ],
})
