import CssMinimizerPlugin from 'css-minimizer-webpack-plugin'
import TerserPlugin from 'terser-webpack-plugin'

export default (config) => ({
    ...config,
    optimization: {
        runtimeChunk: true,
        moduleIds: 'deterministic',
        splitChunks: {
            cacheGroups: {
                // Separate vendor bundle
                defaultVendors: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor~main',
                    chunks: (chunk) => chunk.name === 'main',
                    enforce: true,
                },
            },
        },
        minimizer: [
            new TerserPlugin(),
            new CssMinimizerPlugin({
                // https://github.com/postcss/postcss-calc/issues/107
                minimizerOptions: {
                    preset: [
                        'default',
                        {
                            calc: false,
                        },
                    ],
                },
            }),
        ],
    },
})
