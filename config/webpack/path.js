import { join } from 'path'

export const rootPathBuilder = (root) => (...parts) => join(root, ...parts)
export const leadingSlash = (str, add = true) => `${add ? '/' : ''}${str.replace(/^\/+/, '')}`
export const trailingSlash = (str, add = true) => `${str.replace(/\/+$/, '')}${add ? '/' : ''}`
export const slash = (str, add = true) => leadingSlash(trailingSlash(str, add), add)
export const unslash = (str) => slash(str, false)

export default rootPathBuilder
