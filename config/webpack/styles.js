import autoprefixer from 'autoprefixer'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import { loader, plugin } from 'webpack-partial'
import { ext } from './filename.js'

export default (filename) => [
    loader({
        test: /\.styl$/,
        use: [
            {
                loader: MiniCssExtractPlugin.loader,
            },
            {
                loader: 'css-loader',
            },
            {
                loader: 'postcss-loader',
                options: {
                    postcssOptions: {
                        plugins: [
                            autoprefixer(),
                        ],
                    },
                },
            },
            {
                loader: 'stylus-native-loader',
                options: {
                    resolveUrl: true,
                },
            },
        ],
    }),
    plugin(new MiniCssExtractPlugin({
        filename: filename(ext('.css')),
    })),
]
