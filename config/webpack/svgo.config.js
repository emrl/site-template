import { basename, extname } from 'path'

export default ({ inline = false } = {}) => ({
    plugins: [
        inline && {
            name: 'customInline',
            fn: (_item, _params, info) => ({
                element: {
                    enter(node) {
                        const classList = new Set(
                            node.attributes.class == null ? null : node.attributes.class.split(' ')
                        )

                        if (node.name === 'svg') {
                            // Set CSS class name for including inline
                            const name = basename(info.path, extname(info.path))
                            classList.add(`icon icon-${name}`)
                            delete node.attributes.id
                        }

                        if (node.attributes.id) {
                            // Convert IDs into classes. Illustrator appends "_{n}"
                            // to duplicate layer names, so we strip that off
                            const [name] = node.attributes.id.split('_')
                            classList.add(`icon-part-${name}`)
                            delete node.attributes.id
                        }

                        node.attributes.class = Array.from(classList).join(' ')
                    },
                },
            }),
        },
        {
            name: 'preset-default',
            params: {
                overrides: {
                    removeViewBox: false,
                    cleanupIds: inline,
                    collapseGroups: inline,
                },
            },
        },
        inline && 'removeXMLNS',
    ].filter(Boolean)
})
