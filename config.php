<?php

declare(strict_types=1);

use Dotenv\Dotenv;

use function Site\Core\env;
use function Site\Core\is_env;

(new Site\Core\Environment(Dotenv::createImmutable(__DIR__)))->load();

// Custom
define('ROOT_DIR', __DIR__);
define('PUBLIC_DIR', ROOT_DIR.'/public');
define('DEBUGGING', isset($_GET['debug']));
define('GOOGLE_API_KEY', env('GOOGLE_API_KEY', ''));
define('HOT_FILE', PUBLIC_DIR.'/c/dist/hot');
define('MANIFEST_FILE', PUBLIC_DIR.'/c/dist/manifest.json');
define('SMTP_HOST', env('SMTP_HOST'));
define('SMTP_USERNAME', env('SMTP_USERNAME'));
define('SMTP_PASSWORD', env('SMTP_PASSWORD'));
define('SMTP_PORT', env('SMTP_PORT'));
define('SMTP_SECURE', env('SMTP_SECURE'));
define('SMTP_AUTH', env('SMTP_AUTH'));

// Third-party
define('GF_LICENSE_KEY', env('GF_LICENSE_KEY'));

// Built-in
define('WP_ENVIRONMENT_TYPE', env('WP_ENVIRONMENT_TYPE', 'production'));
define('WP_DEVELOPMENT_MODE', is_env('local', 'development') ? 'all' : '');
define('WP_HOME', env('APP_URL'));
define('WP_SITEURL', WP_HOME.'/w');
define('WP_CONTENT_DIR', PUBLIC_DIR.'/c');
define('WP_CONTENT_URL', WP_HOME.'/c');
define('DB_HOST', env('DB_HOST', 'localhost'));
define('DB_NAME', env('DB_DATABASE'));
define('DB_USER', env('DB_USERNAME'));
define('DB_PASSWORD', env('DB_PASSWORD'));
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', 'utf8mb4_unicode_ci');
define('AUTH_KEY', env('AUTH_KEY'));
define('SECURE_AUTH_KEY', env('SECURE_AUTH_KEY'));
define('LOGGED_IN_KEY', env('LOGGED_IN_KEY'));
define('NONCE_KEY', env('NONCE_KEY'));
define('AUTH_SALT', env('AUTH_SALT'));
define('SECURE_AUTH_SALT', env('SECURE_AUTH_SALT'));
define('LOGGED_IN_SALT', env('LOGGED_IN_SALT'));
define('NONCE_SALT', env('NONCE_SALT'));
define('WPLANG', '');
define('DISALLOW_FILE_EDIT', true);
define('DISALLOW_FILE_MODS', !is_env('local', 'development'));
define('WP_DEBUG', !is_env('production') || DEBUGGING);
define('WP_DEBUG_DISPLAY', WP_DEBUG);
define('SAVEQUERIES', DEBUGGING);
define('SCRIPT_DEBUG', DEBUGGING);
define('WP_CACHE', true);
define('DISABLE_WP_CRON', is_env('production'));

if (!defined('ABSPATH')) {
    define('ABSPATH', PUBLIC_DIR.'/w/');
}

$table_prefix = 'em_';
