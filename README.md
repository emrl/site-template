# Site template

## Initial setup

Copy `.env.example` to `.env`.

Set a variable for your development hostname/domain (ie: `mysite.test`):

```bash
$ TLD=mysite.test
```

Replace `site-template.test` with your development domain in `.env`.

```bash
$ sed -i "s/site-template.test/$TLD/g" .env
```

Install [mkcert](https://mkcert.dev/) if you haven't already. Then generate the SSL certificates:

```bash
$ mkcert -key-file server.key -cert-file server.crt "$TLD"
```

Update your local hosts file and add entry for domain

```bash
$ echo -e "127.0.0.1 $TLD" | sudo tee -a /etc/hosts
```

## Developing with Docker

Update `.env` with database variables. Make sure the auth keys and salts are not empty.

```bash
DB_HOST=mysql
DB_DATABASE=dev
DB_USERNAME=dev
DB_PASSWORD=dev
```

First you will need to make sure you have credentials saved for [Advanced Custom Fields](https://www.advancedcustomfields.com/resources/installing-acf-pro-with-composer/). If not, you can use the following command (replace `ACF_KEY` with your actual license key):

```bash
$ docker compose run --rm --user application --entrypoint composer php config --global --auth http-basic.connect.advancedcustomfields.com ACF_KEY "https://$TLD"
```

Install dependencies

```bash
$ docker compose run --rm --user application --entrypoint composer php install
$ docker compose run --rm --user node node npm install
$ docker compose run --rm python pip install -r requirements.txt
```

Start containers:

```bash
$ docker compose up
```

Docker will start the following containers by default:

Container | Purpose
-|-
`php` | Apache & PHP, runs application
`mysql` | MySQL database
`node` | Runs webpack build process on port 8080

Now you can view the site at the domain you specified previously.

> ℹ️ The Python container for mkdocs does not start by default. You can run it along with the other containers or independently.

```bash
# Start all containers
$ docker compose --profile docs up

# Or independently
$ docker compose up python
```

Container | Purpose
-|-
`python` | Runs mkdocs build process at http://localhost:8000

To allow sending of email, you can start the MailHog container as well.

> ℹ️ The MailHog container does not start by default. You can run it along with the other containers or independently.

```bash
# Start with all containers
$ docker compose --profile mail up

# Or independently
$ docker compose up mailhog
```

### Running commands through Docker

Example: use Deployer to pull in content from staging site:

```bash
# If container is already running
$ docker compose exec --user application php vendor/bin/dep content:pull stage=staging

# If container has stopped
$ docker compose run --rm --user application --entrypoint /app/vendor/bin/dep php content:pull stage=staging
```

## Fluid spacing

See https://utopia.fyi/clamp/calculator for generating CSS `clamp()` values.

[Spacing](https://utopia.fyi/clamp/calculator?a=320,1440,8—8|8—16|16—32|16—48|36—64|36—96|64—1488)

## Files

### Root directory

File | Description
-|-
[`/.editorconfig`](.editorconfig) | [EditorConfig](https://editorconfig.org/)
[`/.env.example`](.env.example) | Example environment variables
[`/.gitignore`](.gitignore) | Files to exclude from Git
[`/.php-cs-fixer.dist.php`](.php-cs-fixer.dist.php) | [PHP Coding Standards Fixer](https://cs.symfony.com/) configuration
[`/.phpcs.xml.dist`](.phpcs.xml.dist) | [PHP_Codesniffer](https://github.com/squizlabs/PHP_CodeSniffer) configuration
[`/app.php`](app.php) | Application bootstrap
[`/bitbucket-pipelines.yml`](bitbucket-pipelines.yml) | [Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html) configuration
[`/composer.json`](composer.json) | [Composer](https://getcomposer.org/) configuration
[`/config.php`](config.php) | WordPress configuration
[`/deploy.php`](deploy.php) | [Deployer](https://deployer.org/) configuration
[`/docker-compose.yml`](docker-compose.yml) | [Docker Compose](https://docs.docker.com/compose/) configuration
[`/mkdocs.yml`](mkdocs.yml) | [MkDocs](https://www.mkdocs.org/) configuration
[`/package.json`](package.json) | [npm](https://www.npmjs.com/) configuration
[`/phpunit.xml.dist`](phpunit.xml.dist) | [PHPUnit](https://phpunit.de/) configuration
[`/requirements.txt`](requirements.txt) | [pip](https://pip.pypa.io/en/stable/) configuration, for docs
[`/webpack.config.js`](webpack.config.js) | [webpack](https://webpack.js.org/) configuration
[`/wp-cli.yml`](wp-cli.yml) | [WP-CLI](http://wp-cli.org/) configuration

### Config directory

The `/config/` directory contains any configuration files.

### Docs directory

The `/docs/` directory holds documentation source files that are eventually built into the `/public/kb/` directory.

### Public directory

The `/public/` directory is the server's document root.

File | Description
-|-
`/public/c/` | WordPress content directory
`/public/w/` | [WordPress core](https://codex.wordpress.org/Giving_WordPress_Its_Own_Directory), installed by Composer
[`/public/.htaccess`](public/.htaccess) | Apache server configuration
[`/public/index.php`](public/index.php) | Entry point
[`/public/manifest.webmanifest`](public/manifest.webmanifest) | [Manifest file](https://developer.mozilla.org/en-US/docs/Web/Manifest)
[`/public/wp-config.php`](public/wp-config.php) | WordPress configuration file. This simply includes `config.php`

### Resources directory

Source files for scripts, styles, images, icons, and other media. Files are packaged by webpack and output to `/public/c/dist/`.

### Kb directory

The `/public/kb/` directory serves the Knowledge Base. It is created by MkDocs and the result of building the source files in the `/docs/` directory.

### Plugins directory

The `/public/c/plugins/` directory stores plugins. Since Composer can manage all plugins in the [WordPress Plugin Repository](https://wordpress.org/plugins/), all files in this directory are ignored by Git.

If a commercial plugin is to be used, it will need to be unignored in the `/.gitignore` file.

### Mu-plugins directory

The `/public/c/mu-plugins/` directory stores [Must Use Plugins](https://codex.wordpress.org/Must_Use_Plugins).

File | Description
-|-
[`/public/c/mu-plugins/advanced-custom-fields-pro.php`](public/c/mu-plugins/advanced-custom-fields-pro.php) | Loads Advanced Custom Fields plugin
[`/public/c/mu-plugins/commands.php`](public/c/mu-plugins/commands.php) | Registers WP-CLI commands
[`/public/c/mu-plugins/error-reporting.php`](public/c/mu-plugins/error-reporting.php) | Turns off error reporting for deprecation errors coming from WordPress core or plugins
[`/public/c/mu-plugins/register-theme-directory.php`](public/c/mu-plugins/register-theme-directory.php) | Registers custom theme directory, along with WordPress' default theme directory

### Src directory

The `/src/` directory contains custom functionality for the site (post types, theme setup, etc).

### Tests directory

The `/tests/` directory contains PHP unit tests.

### Theme directory

The `/theme/` directory acts as the WordPress theme.
