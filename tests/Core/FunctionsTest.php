<?php

declare(strict_types=1);

namespace Site\Tests\Core;

use PHPUnit\Framework\TestCase;

use function Site\Core\env;
use function Site\Core\is_env;

final class FunctionsTest extends TestCase
{
    public function testEnv(): void
    {
        $this->assertNull(
            env('TEST'),
            'Null returned for missing',
        );

        $this->assertSame(
            'test',
            env('TEST', 'test'),
            'Default returned for missing',
        );

        $this->assertSame(
            'test',
            env('TEST', function (): string {
                return 'test';
            }),
            'Default callable is called for missing',
        );

        $_ENV['TEST'] = 'test';

        $this->assertSame(
            'test',
            env('TEST', 'other'),
            'Value is returned from environment',
        );

        unset($_ENV['TEST']);
    }

    public function testIsEnv(): void
    {
        $_ENV['WP_ENVIRONMENT_TYPE'] = 'staging';
        $this->assertTrue(is_env('staging'));
        $this->assertTrue(is_env('development', 'staging'));
        $this->assertFalse(is_env('production'));
        unset($_ENV['WP_ENVIRONMENT_TYPE']);
    }
}
