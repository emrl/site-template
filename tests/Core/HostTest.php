<?php

declare(strict_types=1);

namespace Site\Tests\Core;

use PHPUnit\Framework\TestCase;
use Site\Core\Host;

final class HostTest extends TestCase
{
    protected function tearDown(): void
    {
        unset(
            $_SERVER['HTTPS'],
            $_SERVER['HTTP_HOST'],
            $_SERVER['HTTP_X_FORWARDED_HOST'],
            $_SERVER['HTTP_X_FORWARDED_PROTO'],
        );
    }

    public function testDetectProxy(): void
    {
        $_SERVER['HTTP_X_FORWARDED_HOST'] = 'domain.com';
        $_SERVER['HTTP_X_FORWARDED_PROTO'] = 'https';
        (new Host())->detectProxy();
        $this->assertSame($_SERVER['HTTP_X_FORWARDED_HOST'], $_SERVER['HTTP_HOST']);
        $this->assertSame('on', $_SERVER['HTTPS']);
    }

    public function testHost(): void
    {
        $host = new Host();
        $this->assertSame(gethostname(), $host->host());

        $_SERVER['HTTP_HOST'] = 'domain.com/';
        $this->assertSame('domain.com', $host->host());
    }

    public function testProtocol(): void
    {
        $host = new Host();
        $this->assertSame('http://', $host->protocol());

        $_SERVER['HTTPS'] = 'off';
        $this->assertSame('http://', $host->protocol());

        $_SERVER['HTTPS'] = 'on';
        $this->assertSame('https://', $host->protocol());
    }
}
