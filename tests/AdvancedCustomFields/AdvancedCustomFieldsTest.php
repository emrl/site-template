<?php

declare(strict_types=1);

namespace Site\Tests\AdvancedCustomFields;

use Site\Integration\AdvancedCustomFields\AdvancedCustomFields;
use Site\Tests\TestCase;

use function Brain\Monkey\Functions\when;

final class AdvancedCustomFieldsTest extends TestCase
{
    public function testAddsFilters(): void
    {
        $instance = (new AdvancedCustomFields())->register();
        $this->assertNotFalse(has_filter('acf/fields/relationship/query', [$instance, 'relationshipQuery']));
    }

    public function testFilter(): void
    {
        when('get_post_type')->justReturn('post');

        $instance = new AdvancedCustomFields();

        $this->assertSame(
            [
                'post_type' => 'post',
                'post__not_in' => [1],
            ],
            $instance->relationshipQuery(['post_type' => 'post'], [], 1),
            'Single post type passed',
        );

        $this->assertSame(
            [
                'post_type' => ['page', 'post'],
                'other_arg' => true,
                'post__not_in' => [2, 1],
            ],
            $instance->relationshipQuery([
                'post_type' => ['page', 'post'],
                'other_arg' => true,
                'post__not_in' => [2],
            ], [], 1),
            'Array of post types and `post__not_in`',
        );

        $same = ['post_type' => ['other', 'types']];

        $this->assertSame(
            $same,
            $instance->relationshipQuery($same, [], 1),
            'Other post type left as is',
        );

        $this->assertSame(
            $same,
            $instance->relationshipQuery($same, [], 'category_1'),
            'Non numeric ID left as is',
        );
    }
}
