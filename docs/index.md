# [Site Name] Documentation

Hi!

Welcome to the docs. Here you'll find information that helps you customize **yourwebsite.com**. Use any of the links to the left to jump straight in, or use the search bar in the top right corner to find what you're looking for.

Your site uses [Wordpress](https://wordpress.org), which is an [open source](https://en.wikipedia.org/wiki/Open-source_software) [CMS](https://en.wikipedia.org/wiki/Content_management_system) that allows you to interact with and make changes to your website. 

The docs in here are specific to **your** site! For general WordPress help please visit the official [Wordpress documentation](https://codex.wordpress.org/).

### Access Your Admin

You can access your Wordpress Admin page anytime by heading to [**yoursitename.com/w**](https://yoursitename.com/w).

### Need Help?

Your site offers live chat support! Or, you can always email the EMRL nerds at [dev@emrl.com](mailto:dev@emrl.com). 
