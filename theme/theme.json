{
    "$schema": "https://schemas.wp.org/trunk/theme.json",
    "version": 3,
    "settings": {
        "appearanceTools": true,
        "useRootPaddingAwareAlignments": true,
        "layout": {
            "contentSize": "42rem",
            "wideSize": "78rem"
        },
        "spacing": {
            "units": [
                "%",
                "px",
                "em",
                "rem",
                "vh",
                "vw"
            ],
            "defaultSpacingSizes": false,
            "spacingSizes": [
                {
                    "name": "Tiny",
                    "slug": "20",
                    "size": ".5rem"
                },
                {
                    "name": "X-Small",
                    "slug": "30",
                    "size": "clamp(0.5rem, 0.3571rem + 0.7143vw, 1rem)"
                },
                {
                    "name": "Small",
                    "slug": "40",
                    "size": "clamp(1rem, 0.7143rem + 1.4286vw, 2rem)"
                },
                {
                    "name": "Regular",
                    "slug": "50",
                    "size": "clamp(1rem, 0.4286rem + 2.8571vw, 3rem)"
                },
                {
                    "name": "Large",
                    "slug": "60",
                    "size": "clamp(2.25rem, 1.75rem + 2.5vw, 4rem)"
                },
                {
                    "name": "X-Large",
                    "slug": "70",
                    "size": "clamp(2.25rem, 1.1786rem + 5.3571vw, 6rem)"
                },
                {
                    "name": "XX-Large",
                    "slug": "80",
                    "size": "clamp(4rem, 2.5rem + 7.5vw, 9.25rem)"
                }
            ]
        },
        "color": {
            "palette": [
                {
                    "name": "Default (dark gray)",
                    "slug": "default",
                    "color": "#212529"
                },
                {
                    "name": "Primary (blue)",
                    "slug": "primary",
                    "color": "#007bff"
                },
                {
                    "name": "Secondary (medium gray)",
                    "slug": "secondary",
                    "color": "#6c757d"
                },
                {
                    "name": "Success (green)",
                    "slug": "success",
                    "color": "#28a745"
                },
                {
                    "name": "Info (cyan)",
                    "slug": "info",
                    "color": "#17a2b8"
                },
                {
                    "name": "Warning (yellow)",
                    "slug": "warning",
                    "color": "#ffc107"
                },
                {
                    "name": "Danger (red)",
                    "slug": "danger",
                    "color": "#dc3545"
                },
                {
                    "name": "Light (light gray)",
                    "slug": "light",
                    "color": "#f8f9fa"
                },
                {
                    "name": "Dark (dark gray)",
                    "slug": "dark",
                    "color": "#333"
                },
                {
                    "name": "Black",
                    "slug": "black",
                    "color": "#000"
                },
                {
                    "name": "White",
                    "slug": "white",
                    "color": "#fff"
                },
                {
                    "name": "Link",
                    "slug": "links",
                    "color": "#007bff"
                }
            ]
        },
        "typography": {
            "fluid": true,
            "defaultFontSizes": false,
            "fontSizes": [
                {
                    "name": "Small",
                    "slug": "small",
                    "size": ".75rem",
                    "fluid": {
                        "min": ".625rem",
                        "max": ".75rem"
                    }
                },
                {
                    "name": "Medium",
                    "slug": "medium",
                    "size": ".875rem",
                    "fluid": {
                        "min": ".75rem",
                        "max": ".875rem"
                    }
                },
                {
                    "name": "Large",
                    "slug": "large",
                    "size": "1.125rem",
                    "fluid": {
                        "min": "1rem",
                        "max": "1.125rem"
                    }
                },
                {
                    "name": "Extra Large",
                    "slug": "x-large",
                    "size": "1.5rem",
                    "fluid": {
                        "min": "1.375rem",
                        "max": "2rem"
                    }
                },
                {
                    "name": "Extra Extra Large",
                    "slug": "xx-large",
                    "size": "4rem",
                    "fluid": {
                        "min": "2rem",
                        "max": "4rem"
                    }
                }
            ],
            "fontFamilies": [
                {
                    "name": "Sans-Serif",
                    "slug": "sans-serif",
                    "fontFamily": "system-ui, \"Segoe UI\", sans-serif"
                },
                {
                    "name": "Serif",
                    "slug": "serif",
                    "fontFamily": "Georgia, serif"
                },
                {
                    "name": "Monospace",
                    "slug": "monospace",
                    "fontFamily": "SFMono-Regular, Menlo, Monaco, Consolas, \"Liberation Mono\", \"Courier New\", monospace"
                }
            ]
        },
        "custom": {
            "breakpoints": {
                "xs": 0,
                "sm": 576,
                "md": 768,
                "stack": "782px",
                "lg": 992,
                "xl": 1260
            }
        }
    },
    "styles": {
        "spacing": {
            "blockGap": "1.2em",
            "padding": {
                "top": "0px",
                "right": "var:preset|spacing|40",
                "bottom": "0px",
                "left": "var:preset|spacing|40"
            }
        },
        "typography": {
            "fontFamily": "var:preset|font-family|sans-serif",
            "fontSize": "var:preset|font-size|large",
            "lineHeight": "1.4"
        },
        "color": {
            "background": "var:preset|color|white",
            "text": "var:preset|color|default"
        },
        "elements": {
            "h1": {
                "typography": {
                    "fontSize": "var:preset|font-size|xx-large",
                    "lineHeight": "1.2"
                }
            },
            "h2": {
                "typography": {
                    "fontSize": "var:preset|font-size|xx-large",
                    "lineHeight": "1.2"
                }
            },
            "h3": {
                "typography": {
                    "fontSize": "var:preset|font-size|x-large",
                    "lineHeight": "1.2"
                }
            },
            "h4": {
                "typography": {
                    "fontSize": "var:preset|font-size|x-large",
                    "lineHeight": "1.2"
                },
                "color": {
                    "text": "var:preset|color|secondary"
                }
            },
            "h5": {
                "typography": {
                    "fontSize": "var:preset|font-size|large",
                    "lineHeight": "1.2"
                }
            },
            "h6": {
                "typography": {
                    "fontSize": "var:preset|font-size|medium",
                    "lineHeight": "1.2"
                }
            },
            "link": {
                "color": {
                    "text": "var:preset|color|links"
                },
                "typography": {
                    "textDecoration": "none"
                },
                ":hover": {
                    "typography": {
                        "textDecoration": "underline"
                    }
                }
            },
            "button": {
                "typography": {
                    "fontSize": "inherit",
                    "fontWeight": "700",
                    "lineHeight": "1.4"
                }
            }
        },
        "blocks": {
            "core/columns": {
                "spacing": {
                    "margin": {
                        "bottom": "0"
                    }
                }
            },
            "core/navigation": {
                "elements": {
                    "link": {
                        "typography": {
                            "textDecoration": "none"
                        },
                        ":hover": {
                            "typography": {
                                "textDecoration": "underline"
                            }
                        },
                        ":focus": {
                            "typography": {
                                "textDecoration": "underline"
                            }
                        }
                    }
                }
            }
        }
    },
    "customTemplates": [],
    "templateParts": [
        {
            "name": "header",
            "title": "Header",
            "area": "header"
        },
        {
            "name": "footer",
            "title": "Footer",
            "area": "footer"
        }
    ]
}
