import $ from 'jquery'

const EXPAND_CLASS = 'is-show'

$(document).on('click', '[data-toggle]', (e) => {
    e.preventDefault()
    const trigger = $(e.currentTarget)
    const action = trigger.data('toggle')
    const target = trigger.data('target') || trigger.attr('href')
    const fragment = trigger.data('fragment')

    switch (action) {
        case 'expand':
            expand(target, fragment)
            break
        case 'collapse':
            collapse(target)
            break
        case '':
            toggle(target, fragment)
    }
})

if (location.hash) {
    expand(location.hash, location.hash)
}

$('[data-toggle][aria-expanded="true"]').each((_, node) => {
    const target = node.dataset.target || node.href
    const fragment = node.dataset.fragment
    expand(target, fragment)
})

function toggle(target, fragment) {
    set(target, !$(target).hasClass(EXPAND_CLASS), fragment)
}

function expand(target, fragment) {
    set(target, true, fragment)
}

function collapse(target) {
    set(target, false)
}

function set(target, expand = true, fragment = '') {
    try {
        const node = $(target)

        node[expand ? 'addClass' : 'removeClass'](EXPAND_CLASS);

        $(`
            [data-toggle][data-target="${target}"],
            [data-toggle][href="${target}"]
        `).attr('aria-expanded', expand)

        if (expand && fragment) {
            location.hash = fragment
        }
    } catch (e) {
        return
    }
}
