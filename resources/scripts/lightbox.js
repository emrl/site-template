import $ from 'jquery'
import 'lity'

$(`
    [href$="jpg"],
    [href$="jpeg"],
    [href$="gif"],
    [href$="png"],
    [href$="svg"],
    [href*="youtube.com/watch"],
    [href*="youtu.be/"],
    [href*="player.vimeo.com/video"]
`).attr('data-lity', '')

$(document).on('lity:open', (e, instance) => {
    if (instance.hasOwnProperty('content')) {
        const classes = instance.content().data('lity-class')

        if (classes) {
            instance.element().addClass(classes)
        }
    }
})
