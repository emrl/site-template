export function throttle(fn, wait = 50) {
    let timer = null

    function later(...args) {
        if ( ! timer) {
            timer = setTimeout(() => {
                fn(...args)
                timer = null
            }, wait)
        }
    }

    later.cancel = () => {
        clearTimeout(timer)
        timer = null
    }

    return later
}
