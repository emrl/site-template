(async () => {
    if (!CSS.supports('text-wrap: balance')) {
        const { default: balanceText } = await import('balance-text')

        balanceText('.is-style-balance-text', { watch: true })
    }
})()
