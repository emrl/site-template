<?php

declare(strict_types=1);

use PhpCsFixer\Config;
use PhpCsFixer\Finder;

return (new Config())
    ->setRules([
        '@Symfony' => true,
        '@Symfony:risky' => true,
        'blank_line_before_statement' => false,
        'global_namespace_import' => [
            'import_classes' => true,
        ],
        'native_constant_invocation' => false,
        'native_function_invocation' => false,
        'no_alternative_syntax' => [
            'fix_non_monolithic_code' => false,
        ],
        'no_extra_blank_lines' => [
            'tokens' => [
                'attribute',
                'break',
                'case',
                'continue',
                'curly_brace_block',
                'default',
                'extra',
                'parenthesis_brace_block',
                'return',
                'square_brace_block',
                'switch',
                'throw',
                'use',
            ],
        ],
        'phpdoc_align' => [
            'align' => 'left',
        ],
        'phpdoc_separation' => false,
        'semicolon_after_instruction' => false,
        'strict_param' => true,
        'yoda_style' => false,
    ])
    ->setRiskyAllowed(true)
    ->setFinder(
        Finder::create()
            ->in([__DIR__.'/src', __DIR__.'/tests', __DIR__.'/theme'])
            ->notPath(['#patterns/.*\.php#'])
            ->append([__FILE__]),
    );
