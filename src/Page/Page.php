<?php

declare(strict_types=1);

namespace Site\Page;

use Fire\Post\Page as PostPage;
use Fire\Post\Type;

class Page extends PostPage
{
    public function register(): Type
    {
        return $this
            ->removeSupport('comments')
            ->addSupport([
                'site/title-display',
            ]);
    }
}
