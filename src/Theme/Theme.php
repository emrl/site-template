<?php

declare(strict_types=1);

namespace Site\Theme;

use Site\Core\Resource;

class Theme
{
    public function __construct(
        protected readonly Resource $resource,
    ) {
    }

    public function register(): self
    {
        return $this
            ->support()
            ->actions()
            ->filters();
    }

    protected function support(): self
    {
        remove_theme_support('core-block-patterns');

        add_theme_support('responsive-embeds');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        add_theme_support('automatic-feed-links');
        add_theme_support('custom-logo');

        add_theme_support('html5', [
            'comment-list',
            'comment-form',
            'search-form',
            'gallery',
            'caption',
            'style',
            'script',
        ]);

        return $this;
    }

    protected function actions(): self
    {
        remove_action('welcome_panel', 'wp_welcome_panel');
        remove_action('wp_head', 'wp_generator');

        add_action('wp_head', [$this, 'head']);
        add_action('wp_enqueue_scripts', [$this, 'scripts']);

        return $this;
    }

    protected function filters(): self
    {
        add_filter('excerpt_more', fn (): string => '…');
        add_filter('sanitize_html_class', [$this, 'sanitizeHtmlClass'], 10, 2);
        add_filter('safe_svg_optimizer_enabled', fn (): bool => true);

        return $this;
    }

    public function head(): void
    {
        get_template_part('parts/favicons');
    }

    public function scripts(): void
    {
        // Styles
        wp_enqueue_style('site-vendor', $this->resource->url('c/dist/vendor~main.css'), [], null);
        wp_enqueue_style('site-main', $this->resource->url('c/dist/main.css'), ['site-vendor'], null);

        // Scripts
        wp_enqueue_script('site-vendor', $this->resource->url('c/dist/vendor~main.js'), ['jquery'], null, true);

        // Inline the runtime before
        wp_add_inline_script(
            'site-vendor',
            file_get_contents($this->resource->path('c/dist/runtime~main.js')),
            'before',
        );

        wp_enqueue_script('site-main', $this->resource->url('c/dist/main.js'), ['site-vendor'], null, true);

        if (is_singular() && comments_open() && get_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        }
    }

    public function sanitizeHtmlClass(string $sanitized, string $class): string
    {
        return preg_replace('/%[a-fA-F0-9][a-fA-F0-9]|[^A-Za-z0-9:@_-]/', '', $class);
    }
}
