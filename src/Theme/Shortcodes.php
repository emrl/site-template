<?php

declare(strict_types=1);

namespace Site\Theme;

use Site\Core\Resource;

class Shortcodes
{
    public function __construct(
        protected readonly Resource $resource,
    ) {
    }

    public function register(): self
    {
        add_filter('the_content', [$this, 'fix'], 1);
        add_filter('render_block_core/navigation-link', fn (string $content) => do_shortcode($content));
        add_filter('render_block_core/navigation-submenu', fn (string $content) => do_shortcode($content));

        add_shortcode('icon', [$this, 'icon']);

        return $this;
    }

    public function fix(string $content): string
    {
        return strtr($content, [
            '<p>[' => '[',
            ']</p>' => ']',
            ']<br />' => ']',
        ]);
    }

    public function icon(string|array $attrs): string
    {
        $icon = $attrs[0] ?? '';

        return (string) @file_get_contents($this->resource->path("c/dist/$icon.svg"));
    }

    public function user(string|array $attrs): string
    {
        $attrs = shortcode_atts([
            'key' => '',
            'fallback' => '',
            'hide_if_empty' => false,
        ], $attrs, 'user');

        if ($attrs['key'] === 'user_pass') {
            return '';
        }

        $attrs['hide_if_empty'] = filter_var($attrs['hide_if_empty'], FILTER_VALIDATE_BOOLEAN);

        $id = get_current_user_id();
        $value = '';

        if ($id) {
            $data = get_userdata($id);
            $value = $data->data->{trim(html_entity_decode($attrs['key']), " \n\r\t\v\0\"'")} ?? '';
        }

        if (!$id || !$value) {
            if ($attrs['hide_if_empty']) {
                return '';
            }

            return $attrs['fallback'];
        }

        return (string) $value;
    }
}
