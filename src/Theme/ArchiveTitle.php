<?php

declare(strict_types=1);

namespace Site\Theme;

use WP_Query;

class ArchiveTitle
{
    public function register(): self
    {
        add_action('pre_get_posts', [$this, 'enableArchiveForHome']);
        add_action('pre_get_posts', [$this, 'enableArchiveForSearch']);

        add_filter('get_the_archive_title', [$this, 'archiveTitle'], 11);

        return $this;
    }

    public function enableArchiveForHome(WP_Query $query): void
    {
        if (is_home() && $query->is_main_query()) {
            $query->is_archive = true;
        }
    }

    public function enableArchiveForSearch(WP_Query $query): void
    {
        if (is_search() && $query->is_main_query()) {
            $query->is_archive = true;
        }
    }

    public function archiveTitle(string $title): string
    {
        if (is_home()) {
            $title = get_the_title(get_option('page_for_posts')) ?: $title;
        }

        return $title;
    }
}
