import {
    InspectorControls,
    RichText,
    store as blockEditorStore,
    __experimentalGetBorderClassesAndStyles as getBorderClassesAndStyles,
    __experimentalGetColorClassesAndStyles as getColorClassesAndStyles,
    useBlockProps,
    __experimentalUseBorderProps as useBorderProps,
    __experimentalUseColorProps as useColorProps,
    useInnerBlocksProps,
    __experimentalGetSpacingClassesAndStyles as useSpacingProps,
    getTypographyClassesAndStyles as useTypographyProps
} from '@wordpress/block-editor'
import { registerBlockType } from '@wordpress/blocks'
import { PanelBody, TextControl, ToggleControl } from '@wordpress/components'
import { useSelect } from '@wordpress/data'
import classNames from 'classnames'
import metadata from './block.json'
import './editor.styl'

const TEMPLATE = [
    [
        'core/paragraph',
        {
            placeholder: 'Type / to add a hidden block',
        },
    ],
]

registerBlockType(metadata.name, {
    edit({ attributes, setAttributes, clientId }) {
        const { open, name, summary } = attributes

        const hasSelection = useSelect((select) => {
            const { isBlockSelected, hasSelectedInnerBlock } = select(blockEditorStore)

            return (hasSelectedInnerBlock(clientId, true) || isBlockSelected(clientId))
        })

        const blockProps = useBlockProps({ open: hasSelection || open })

        const { children, ...innerBlocksProps } = useInnerBlocksProps(blockProps, {
            template: TEMPLATE,
            __experimentalCaptureToolbars: true,
        })

        const borderProps = useBorderProps(attributes)
        const colorProps = useColorProps(attributes)
        const spacingProps = useSpacingProps(attributes)
        const typographyProps = useTypographyProps(attributes)

        return (
            <>
                <InspectorControls>
                    <PanelBody title="Settings">
                        <ToggleControl
                            label="Open by default"
                            checked={ open }
                            onChange={ () => setAttributes({ open: !open }) }
                        />
                        <TextControl
                            label="Name"
                            help="When this attribute is used, multiple Details Summary blocks that have the same name value form a semantic group and they will behave as an exclusive accordion. When you open one of the elements from the group, the previously opened one will automatically close."
                            value={ name }
                            onChange={ (value) => setAttributes({ name: value }) }
                        />
                    </PanelBody>
                </InspectorControls>
                <details { ...innerBlocksProps }>
                    <summary
                        onClick={ (e) => e.preventDefault() }
                        className={ classNames(
                            colorProps.className,
                            borderProps.className,
                            typographyProps.className,
                        ) }
                        style={ {
                            ...borderProps.style,
                            ...colorProps.style,
                            ...spacingProps.style,
                            ...typographyProps.style,
                        } }
                    >
                        <RichText
                            aria-label="Write summary"
                            placeholder="Write summary…"
                            value={ summary }
                            onChange={ (value) => setAttributes({ summary: value }) }
                            multiline={ false }
                        />
                    </summary>
                    <div className="wp-block-site-details-summary-children">
                        { children }
                    </div>
                </details>
            </>
        )
    },
    save: ({ attributes } ) => {
        const { open, name } = attributes
        const summary = attributes.summary ? attributes.summary : 'Details'
        const blockProps = useBlockProps.save({ open, name: name || false })
        const { children, ...innerBlocksProps } = useInnerBlocksProps.save(blockProps)
        const borderProps = getBorderClassesAndStyles(attributes)
        const colorProps = getColorClassesAndStyles(attributes)
        const spacingProps = useSpacingProps(attributes)
        const typographyProps  = useTypographyProps(attributes)

        return (
            <details { ...innerBlocksProps }>
                <summary
                    className={ classNames(
                        borderProps.className,
                        colorProps.className,
                        typographyProps.className,
                    ) }
                    style={ {
                        ...borderProps.style,
                        ...colorProps.style,
                        ...spacingProps.style,
                        ...typographyProps.style,
                    } }
                >
                    <RichText.Content value={ summary } />
                </summary>
                <div className="wp-block-site-details-summary-children">
                    { children }
                </div>
            </details>
        )
    },
})
