import { useBlockProps, useInnerBlocksProps } from '@wordpress/block-editor'
import { createBlock, createBlocksFromInnerBlocksTemplate, registerBlockType } from '@wordpress/blocks'
import metadata from './block.json'
import './editor.styl'

/**
 * @param {Object} attrs
 * @returns {Object}
 */
export const Block = (attrs = {}) => ({
    edit: () => <div { ...useInnerBlocksProps(useBlockProps(), attrs) }></div>,
    save: () => <div { ...useInnerBlocksProps.save(useBlockProps.save()) }></div>,
})

registerBlockType(metadata.name, {
    ...Block({
        template: [
            [ 'site/card-header' ],
            [ 'site/card-body' ],
            [ 'site/card-footer' ],
        ],
        allowedBlocks: [],
    }),
    transforms: {
        from: [
            {
                type: 'block',
                isMultiBlock: true,
                blocks: [ '*' ],
                __experimentalConvert: (blocks) => createBlock(
                    'site/card',
                    {},
                    [
                        createBlock(
                            'site/card-body',
                            {},
                            createBlocksFromInnerBlocksTemplate(blocks.map(({ name, attributes, innerBlocks }) => [
                                name,
                                { ...attributes },
                                innerBlocks,
                            ])),
                        ),
                    ],
                ),
            },
        ],
    },
})
