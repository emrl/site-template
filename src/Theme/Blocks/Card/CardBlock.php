<?php

declare(strict_types=1);

namespace Site\Theme\Blocks\Card;

use Site\Block\Type;

class CardBlock extends Type
{
    protected string $path = __DIR__;
}
