import { Block } from '@/src/Theme/Blocks/Card/editor.js'
import { registerBlockType } from '@wordpress/blocks'
import metadata from './block.json'
import './editor.styl'

registerBlockType(metadata.name, {
    ...Block({
        template: [
            [
                'core/paragraph',
                {
                    placeholder: 'Card footer…',
                },
            ],
        ],
    }),
})
