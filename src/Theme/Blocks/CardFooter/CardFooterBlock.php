<?php

declare(strict_types=1);

namespace Site\Theme\Blocks\CardFooter;

use Site\Block\Type;

class CardFooterBlock extends Type
{
    protected string $path = __DIR__;
}
