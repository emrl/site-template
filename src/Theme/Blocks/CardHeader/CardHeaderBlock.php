<?php

declare(strict_types=1);

namespace Site\Theme\Blocks\CardHeader;

use Site\Block\Type;

class CardHeaderBlock extends Type
{
    protected string $path = __DIR__;
}
