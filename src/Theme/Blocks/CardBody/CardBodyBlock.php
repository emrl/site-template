<?php

declare(strict_types=1);

namespace Site\Theme\Blocks\CardBody;

use Site\Block\Type;

class CardBodyBlock extends Type
{
    protected string $path = __DIR__;
}
