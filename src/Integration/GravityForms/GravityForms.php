<?php

declare(strict_types=1);

namespace Site\Integration\GravityForms;

use GF_Block_Form;
use GF_Field;
use GF_Field_Checkbox;
use GF_Field_MultiSelect;
use GF_Field_Radio;
use GF_Field_Select;
use GFAPI;
use GFFormDisplay;
use RGFormsModel;
use Site\Core\Resource;

use function Fire\Core\filter_merge;

class GravityForms
{
    protected const CHOICE_TYPES = [
        GF_Field_Checkbox::class,
        GF_Field_MultiSelect::class,
        GF_Field_Radio::class,
        GF_Field_Select::class,
    ];

    public function __construct(
        protected readonly Resource $resource,
    ) {
    }

    public function register(): self
    {
        add_filter('render_block', [$this, 'renderBlock'], 10, 2);
        add_filter('gform_pre_render', [$this, 'skipPage']);
        add_filter('gform_ajax_spinner_url', fn (): string => $this->resource->url('c/dist/loading.svg'));
        add_filter('gform_entry_field_value', [$this, 'entryFieldValue'], 10, 4);
        add_filter('gform_entries_field_value', [$this, 'entriesFieldValue'], 10, 4);
        add_filter('wp_editor_settings', [$this, 'editorSettings'], 10, 2);
        add_filter('gform_merge_tag_filter', [$this, 'numericDefaultZero'], 10, 3);

        add_filter('gform_form_args', filter_merge([
            'display_title' => false,
            'display_description' => false,
            'ajax' => true,
        ]));

        return $this;
    }

    /**
     * Gravity Forms outputs multiple "root" elements, so we wrap it all in
     * a container to help with layout flow.
     */
    public function renderBlock(string $content, array $block): string
    {
        if (
            class_exists(GF_Block_Form::class)
            && method_exists(GF_Block_Form::class, 'get_instance')
            && ($instance = GF_Block_Form::get_instance())
            && $block['blockName'] === $instance->type
        ) {
            $content = sprintf('<div class="gravityform-outer-wrapper">%s</div>', $content);
        }

        return $content;
    }

    public function skipPage(array $form): array
    {
        if (
            ($page = rgget('form_page'))
            && !rgpost("is_submit_{$form['id']}")
            && current_user_can('manage_options')
        ) {
            GFFormDisplay::$submission[$form['id']]['page_number'] = $page;
        }

        return $form;
    }

    /**
     * @see https://docs.gravityforms.com/gform_entry_field_value/#2-display-choice-label
     */
    public function entryFieldValue(mixed $value, GF_Field $field, array $entry, array $form): mixed
    {
        if (in_array($field::class, static::CHOICE_TYPES, true)) {
            $value = $field->get_value_entry_detail(RGFormsModel::get_lead_field_value($entry, $field), '', true);
        }

        return $value;
    }

    /**
     * @see https://docs.gravityforms.com/gform_entries_field_value/#2-display-choice-label
     */
    public function entriesFieldValue(mixed $value, int $form_id, int|string $field_id, array $entry): mixed
    {
        $field = GFAPI::get_field($form_id, $field_id);

        if (is_numeric($field_id) && in_array($field::class, static::CHOICE_TYPES, true)) {
            $value = $field->get_value_entry_detail(
                RGFormsModel::get_lead_field_value($entry, $field),
                '',
                true,
                'text',
            );
        }

        return $value;
    }

    /**
     * Disable visual editor as it breaks HTML emails when toggling between visual and text editors.
     */
    public function editorSettings(array $settings, string $id): array
    {
        if ($id === '_gform_setting_message') {
            $settings['tinymce'] = false;
        }

        return $settings;
    }

    public function numericDefaultZero(mixed $value, int|string $id, string $modifier): mixed
    {
        if ($modifier === 'numeric' && empty($value)) {
            $value = 0;
        }

        return $value;
    }
}
