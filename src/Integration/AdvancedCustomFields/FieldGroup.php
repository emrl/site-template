<?php

declare(strict_types=1);

namespace Site\Integration\AdvancedCustomFields;

abstract class FieldGroup
{
    public function register(): self
    {
        // Register after all post types have been registered.
        add_action('init', [$this, 'init'], 11);

        return $this;
    }

    public function init(): void
    {
        if (function_exists('acf_add_local_field_group')) {
            acf_add_local_field_group($this->group());
        }
    }

    abstract protected function group(): array;
}
