<?php

declare(strict_types=1);

namespace Site\Integration\AdvancedCustomFields;

abstract class OptionsPage extends FieldGroup
{
    public function init(): void
    {
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page($this->page());
        }

        parent::init();
    }

    abstract protected function page(): array;
}
