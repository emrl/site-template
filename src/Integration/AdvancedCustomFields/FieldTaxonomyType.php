<?php

declare(strict_types=1);

namespace Site\Integration\AdvancedCustomFields;

use acf_field_select;

class FieldTaxonomyType extends acf_field_select
{
    public function initialize(): void
    {
        parent::initialize();

        $this->name = 'taxonomy_type';
        $this->label = 'Taxonomy Type';
        $this->defaults['return_format'] = 'object';
        $this->defaults['choices'] = acf_get_taxonomy_labels();
    }

    /**
     * @param array $field
     */
    public function render_field_settings($field): void
    {
        // Encode choices (convert from array)
        $field['choices'] = acf_encode_choices($field['choices']);
        $field['default_value'] = acf_encode_choices($field['default_value'], false);

        // default_value
        acf_render_field_setting($field, [
            'label' => __('Default Value', 'acf'),
            'instructions' => '',
            'name' => 'default_value',
            'type' => 'select',
            'choices' => array_merge([
                '' => '',
            ], $this->defaults['choices']),
        ]);

        // allow_null
        acf_render_field_setting($field, [
            'label' => __('Allow Null?', 'acf'),
            'instructions' => '',
            'name' => 'allow_null',
            'type' => 'true_false',
            'ui' => 1,
        ]);

        // multiple
        acf_render_field_setting($field, [
            'label' => __('Select multiple values?', 'acf'),
            'instructions' => '',
            'name' => 'multiple',
            'type' => 'true_false',
            'ui' => 1,
        ]);

        // ui
        acf_render_field_setting($field, [
            'label' => __('Stylised UI', 'acf'),
            'instructions' => '',
            'name' => 'ui',
            'type' => 'true_false',
            'ui' => 1,
        ]);

        // return_format
        acf_render_field_setting($field, [
            'label' => __('Return Format', 'acf'),
            'instructions' => __('Specify the value returned', 'acf'),
            'type' => 'select',
            'name' => 'return_format',
            'choices' => [
                'value' => __('Slug', 'acf'),
                'label' => __('Name', 'acf'),
                'object' => __('Object', 'acf'),
            ],
        ]);
    }

    /**
     * @param int $post_id
     * @param array $field
     */
    public function format_value_single($value, $post_id, $field): mixed
    {
        if (acf_is_empty($value)) {
            return $value;
        }

        switch ($field['return_format']) {
            case 'label':
                $value = acf_maybe_get($field['choices'], $value, $value);
                break;
            case 'object':
                $value = get_taxonomy($value);
                break;
        }

        return $value;
    }
}
