<?php

declare(strict_types=1);

namespace Site\Integration\AdvancedCustomFields;

class AdvancedCustomFields
{
    public function register(): self
    {
        // Register after all post types have been registered.
        add_action('init', [$this, 'types'], 11);

        add_filter('acf/validate_value/type=url', [$this, 'validateUrl'], 11, 2);
        add_filter('acf/fields/relationship/query', [$this, 'relationshipQuery'], 10, 3);

        return $this;
    }

    public function types(): void
    {
        if (!class_exists('acf_field')) {
            return;
        }

        new FieldTaxonomyType();
    }

    public function validateUrl(bool|string $valid, string $value): bool|string
    {
        $protocols = implode('|', array_map('preg_quote', wp_allowed_protocols()));

        if (preg_match("/^($protocols):/", $value)) {
            $valid = true;
        }

        return $valid;
    }

    public function relationshipQuery(array $args, array $field, $id): array
    {
        // Remove current post from relationship fields
        if (!empty($args['post_type']) && is_numeric($id) && ($type = get_post_type($id))) {
            $types = (array) $args['post_type'];

            if (in_array($type, $types, true)) {
                $args['post__not_in'] = (array) ($args['post__not_in'] ?? null);
                $args['post__not_in'][] = $id;
            }
        }

        return $args;
    }
}
