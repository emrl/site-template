<?php

declare(strict_types=1);

namespace Site\Integration\TheEventsCalendar;

use Tribe\Events\Event_Status\Template_Modifications;
use Tribe__Events__Main;

class TheEventsCalendar
{
    public function register(): self
    {
        if (!class_exists(Tribe__Events__Main::class)) {
            return $this;
        }

        add_filter('the_content', [$this, 'addStatusLabel']);
        add_filter('tribe_get_venue_link', fn (string $link): string => strip_tags($link));

        return $this;
    }

    public function addStatusLabel(string $content): string
    {
        if (is_singular(Tribe__Events__Main::POSTTYPE)) {
            $content = tribe(Template_Modifications::class)->add_single_status_reason('', []).$content;
        }

        return $content;
    }
}
