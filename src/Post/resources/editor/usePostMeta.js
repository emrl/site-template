import { useDispatch, useSelect } from '@wordpress/data'

/**
 * @param {String} key
 * @param {mixed} [defaultValue=null]
 * @returns {Array}
 */
export default function usePostMeta(key, defaultValue = null) {
    const value = useSelect((select) => select('core/editor').getEditedPostAttribute('meta')?.[key] ?? defaultValue)
    const dispatch = useDispatch('core/editor')
    const setValue = (newValue) => dispatch.editPost({ meta: { [key]: newValue } })

    return [ value, setValue ]
}
