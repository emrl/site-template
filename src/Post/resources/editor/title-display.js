import {
    PanelRow,
    __experimentalToggleGroupControl as ToggleGroupControl,
    __experimentalToggleGroupControlOption as ToggleGroupControlOption,
} from '@wordpress/components'
import { PluginDocumentSettingPanel } from '@wordpress/edit-post'
import { PostTypeSupportCheck } from '@wordpress/editor'
import { registerPlugin } from '@wordpress/plugins'
import usePostMeta from './usePostMeta.js'

const TITLE_DISPLAY_KEY = 'site/title-display'

function TitleDisplay() {
    const [ value, setValue ] = usePostMeta(TITLE_DISPLAY_KEY, '')

    return (
        <PostTypeSupportCheck supportKeys={ TITLE_DISPLAY_KEY }>
            <PluginDocumentSettingPanel name="site-post-title-display" title="Post title">
                <PanelRow>
                    <ToggleGroupControl
                        label="Title display"
                        value={ value }
                        onChange={ setValue }
                    >
                        <ToggleGroupControlOption value="" label="Show" />
                        <ToggleGroupControlOption value="hide" label="Hide" />
                    </ToggleGroupControl>
                </PanelRow>
            </PluginDocumentSettingPanel>
        </PostTypeSupportCheck>
    )
}

registerPlugin('plugin-post-title-display', {
    render: TitleDisplay,
})
