<?php

declare(strict_types=1);

namespace Site\Post;

use Fire\Post\Post as PostPost;
use WP_Post;

use function Fire\Core\filter_merge;

class Post extends PostPost
{
    public function register(): self
    {
        add_action('init', [$this, 'addRandomOrderForRest'], 11);

        add_filter('body_class', [$this, 'bodyClass']);
        add_filter('post_thumbnail_id', [$this, 'postThumbnailId'], 10, 2);
        add_filter('site/options/fields', filter_merge($this->siteOptionsFieldsForPostThumbnail()));

        return $this
            ->registerMeta()
            ->addSupport([
                'site/title-display',
            ]);
    }

    protected function registerMeta(): self
    {
        register_post_meta(
            '',
            'site/title-display',
            [
                'type' => 'string',
                'description' => 'Determines whether to show the page title or not',
                'single' => true,
                'default' => '',
                'sanitize_callback' => fn (mixed $value): string => $value === 'hide' ? $value : '',
                'auth_callback' => fn (): bool => current_user_can('edit_posts'),
                'show_in_rest' => true,
            ],
        );

        return $this;
    }

    public function addRandomOrderForRest(): self
    {
        foreach (get_post_types(['show_in_rest' => true]) as $type) {
            add_filter("rest_{$type}_collection_params", function (array $params): array {
                $params['orderby']['enum'][] = 'rand';
                return $params;
            });
        }

        return $this;
    }

    public function bodyClass(array $classes): array
    {
        if (is_singular() && !show_title()) {
            $classes[] = 'has-page-title-hidden';
        }

        return $classes;
    }

    public function postThumbnailId(int $id, WP_Post $post): int
    {
        if (!$id && $this->isType($post->post_type) && ($fallback = featured_image_fallback())) {
            $id = $fallback;
        }

        return $id;
    }

    public function siteOptionsFieldsForPostThumbnail(): array
    {
        return [
            // Posts tab
            [
                'key' => 'field_site_options_posts_tab',
                'type' => 'tab',
                'label' => 'Posts',
            ],
            // Featured image fallback
            [
                'key' => 'field_site_options_featured_image_fallback',
                'name' => 'site_options_featured_image_fallback',
                'type' => 'image',
                'label' => 'Featured image fallback',
                'return_format' => 'id',
            ],
        ];
    }
}
