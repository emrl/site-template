<?php

declare(strict_types=1);

namespace Site\Post\Blocks\Post;

use Site\Integration\AdvancedCustomFields\FieldGroup;
use WP_Post_Type;

class BlockFieldGroup extends FieldGroup
{
    protected function group(): array
    {
        return [
            'key' => 'group_post_block',
            'title' => 'Post',
            'location' => [
                [
                    [
                        'param' => 'block',
                        'operator' => '==',
                        'value' => 'site/post',
                    ],
                ],
            ],
            'fields' => [
                [
                    'key' => 'field_post_block_query_type',
                    'name' => 'query_type',
                    'type' => 'radio',
                    'label' => 'Query type',
                    'required' => true,
                    'choices' => [
                        '' => 'Specific post',
                        'latest' => 'Latest',
                        'random' => 'Random',
                    ],
                    'default_value' => '',
                ],
                [
                    'key' => 'field_post_block_post_type',
                    'name' => 'post_type',
                    'type' => 'select',
                    'label' => 'Post type',
                    'required' => true,
                    'choices' => array_reduce(
                        get_post_types(['show_ui' => true], 'objects'),
                        function (array $carry, WP_Post_Type $type): array {
                            $carry[$type->name] = $type->labels->singular_name;
                            return $carry;
                        },
                        ['any' => 'Any'],
                    ),
                    'default_value' => 'any',
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'field_post_block_query_type',
                                'operator' => '==',
                                'value' => 'latest',
                            ],
                        ],
                        [
                            [
                                'field' => 'field_post_block_query_type',
                                'operator' => '==',
                                'value' => 'random',
                            ],
                        ],
                    ],
                ],
                [
                    'key' => 'field_post_block_post',
                    'name' => 'post',
                    'type' => 'post_object',
                    'label' => 'Post',
                    'required' => true,
                    'return_format' => 'object',
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'field_post_block_query_type',
                                'operator' => '==',
                                'value' => '',
                            ],
                        ],
                    ],
                ],
                [
                    'key' => 'field_post_block_show_title',
                    'name' => 'show-title',
                    'type' => 'true_false',
                    'label' => 'Title',
                    'message' => 'Show title',
                    'default_value' => true,
                ],
                [
                    'key' => 'field_post_block_show_excerpt',
                    'name' => 'show-excerpt',
                    'type' => 'true_false',
                    'label' => 'Excerpt',
                    'message' => 'Show excerpt',
                    'default_value' => true,
                ],
                [
                    'key' => 'field_post_block_show_content',
                    'name' => 'show-content',
                    'type' => 'true_false',
                    'label' => 'Content',
                    'message' => 'Show content',
                    'default_value' => false,
                ],
                [
                    'key' => 'field_post_block_show_thumbnail',
                    'name' => 'show-thumbnail',
                    'type' => 'true_false',
                    'label' => 'Thumbnail',
                    'message' => 'Show thumbnail',
                    'default_value' => false,
                ],
                [
                    'key' => 'field_post_block_make_link',
                    'name' => 'make-link',
                    'type' => 'true_false',
                    'label' => 'Link',
                    'message' => 'Make title and/or thumbnail link to post',
                    'default_value' => true,
                ],
            ],
        ];
    }
}
