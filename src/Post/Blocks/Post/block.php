<?php

declare(strict_types=1);

use Fire\Query\Iterator;

$class = 'wp-block-post '.($block['className'] ?? '');

if (!empty($block['align'])) {
    $class .= ' align'.$block['align'];
}

$query_type = get_field('field_post_block_query_type');
$query_args = [];

// Specific post
if ($query_type === '') {
    $post = get_field('field_post_block_post');

    if (!$post) {
        if ($is_preview) {
            printf('
                <div class="%s" style="color: #ffc107; padding: 8px;">
                    <strong>Select a post</strong>
                </div>
            ', esc_attr($class));
        }

        return;
    }

    $query_args = [
        'p' => $post->ID,
        'post_type' => $post->post_type,
    ];
} else {
    $query_args = [
        'posts_per_page' => 1,
        'post_type' => get_field('field_post_block_post_type'),
    ];

    if ($query_type === 'random') {
        $query_args['orderby'] = 'rand';
    }
}

$show_title = get_field('field_post_block_show_title');
$show_excerpt = get_field('field_post_block_show_excerpt');
$show_content = get_field('field_post_block_show_content');
$show_thumbnail = get_field('field_post_block_show_thumbnail');
$make_link = get_field('field_post_block_make_link');

$query = new WP_Query($query_args);

if (!$query->have_posts()) {
    if ($is_preview) {
        printf('
            <div class="%s" style="color: #ffc107; padding: 8px;">
                <strong>No posts found</strong>
            </div>
        ', esc_attr($class));
    }

    return;
}

$posts = new Iterator($query);

?>
<?php foreach ($posts as $post): ?>
    <div class="<?php echo esc_attr($class) ?>">
        <?php if ($show_thumbnail && has_post_thumbnail()): ?>
            <?php if ($make_link): ?>
                <a href="<?php the_permalink() ?>">
                    <?php the_post_thumbnail('large', ['class' => 'wp-block-post__thumbnail']) ?>
                </a>
            <?php else: ?>
                <?php the_post_thumbnail('large', ['class' => 'wp-block-post__thumbnail']) ?>
            <?php endif ?>
        <?php endif ?>
        <?php if ($show_title): ?>
            <h3 class="wp-block-post__title">
                <?php if ($make_link): ?>
                    <a href="<?php the_permalink() ?>" class="has-display-block is-style-balance-text">
                        <?php the_title() ?>
                    </a>
                <?php else: ?>
                    <?php the_title() ?>
                <?php endif ?>
            </h3>
        <?php endif ?>
        <?php if ($show_excerpt): ?>
            <div class="wp-block-post__excerpt">
                <?php the_excerpt() ?>
            </div>
        <?php endif ?>
        <?php if ($show_content): ?>
            <div class="wp-block-post__content">
                <?php the_content() ?>
            </div>
        <?php endif ?>
    </div>
<?php endforeach ?>
