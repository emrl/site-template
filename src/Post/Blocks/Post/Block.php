<?php

declare(strict_types=1);

namespace Site\Post\Blocks\Post;

use Site\Block\Type;

class Block extends Type
{
    protected string $path = __DIR__;
}
