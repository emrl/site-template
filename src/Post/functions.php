<?php

declare(strict_types=1);

namespace Site\Post;

/**
 * Test whether title should show.
 */
function show_title(?int $id = null): bool
{
    return get_post_meta($id ?: get_the_ID(), 'site/title-display', true) === '';
}

/**
 * Featured image fallback/default.
 */
function featured_image_fallback(): int
{
    return (int) get_field('field_site_options_featured_image_fallback', 'options');
}
