<?php

declare(strict_types=1);

namespace Site\Editor;

use Site\Core\Resource;

use function Fire\Core\filter_merge;
use function Site\Core\is_env;

class Editor
{
    public function __construct(
        protected readonly Resource $resource,
    ) {
    }

    public function register(): self
    {
        add_action('enqueue_block_editor_assets', [$this, 'assets']);

        if (is_env('local', 'development')) {
            add_filter('style_loader_tag', [$this, 'styleLoaderTag'], 10, 2);
        }

        // https://www.npmjs.com/package/@emrl/webpack-css-class-extract-plugin
        if (is_file($path = $this->resource->path('c/dist/main.json'))) {
            // https://wordpress.org/plugins/block-class-autocomplete
            add_filter(
                'block-class-autocomplete/suggestions',
                filter_merge(json_decode(file_get_contents($path), true)),
            );
        }

        return $this->blockStyles();
    }

    public function assets(): void
    {
        $asset = [
            'dependencies' => [],
            'version' => '',
        ];

        // https://www.npmjs.com/package/@wordpress/dependency-extraction-webpack-plugin
        if (is_file($path = $this->resource->path('c/dist/editor.php'))) {
            $asset = require $path;
        }

        wp_enqueue_script(
            'site-editor-runtime',
            $this->resource->url('c/dist/runtime~editor.js'),
            $asset['dependencies'],
            $asset['version'],
        );

        wp_enqueue_script(
            'site-editor',
            $this->resource->url('c/dist/editor.js'),
            ['site-editor-runtime'],
        );

        wp_enqueue_style('site-editor', $this->resource->url('c/dist/editor.css'));
    }

    /**
     * Enable cross origin reading of stylesheet when webpack dev server is running.
     * This is needed because WordPress excludes stylesheets that it cannot read
     * from being used in the site editor iframes.
     *
     * @see https://github.com/WordPress/gutenberg/issues/38673
     */
    public function styleLoaderTag(string $tag, string $handle): string
    {
        if ($handle === 'site-editor') {
            $tag = str_replace('href=', 'crossorigin href=', $tag);
        }

        return $tag;
    }

    protected function blockStyles(): self
    {
        register_block_style('core/buttons', [
            'name' => 'group',
            'label' => 'Group',
        ]);

        register_block_style('core/button', [
            'name' => 'link',
            'label' => 'Link',
        ]);

        register_block_style('core/post-excerpt', [
            'name' => 'balance-text',
            'label' => 'Balance text',
        ]);

        register_block_style('core/post-excerpt', [
            'name' => 'pretty-text',
            'label' => 'Pretty text',
        ]);

        register_block_style('core/gallery', [
            'name' => 'logos',
            'label' => 'Logos',
        ]);

        register_block_style('core/heading', [
            'name' => 'balance-text',
            'label' => 'Balance text',
        ]);

        register_block_style('core/heading', [
            'name' => 'pretty-text',
            'label' => 'Pretty text',
        ]);

        register_block_style('core/list', [
            'name' => 'simple',
            'label' => 'Simple',
        ]);

        register_block_style('core/list', [
            'name' => 'columns',
            'label' => 'Columns',
        ]);

        register_block_style('core/list', [
            'name' => 'group',
            'label' => 'Group',
        ]);

        register_block_style('core/paragraph', [
            'name' => 'balance-text',
            'label' => 'Balance text',
        ]);

        register_block_style('core/paragraph', [
            'name' => 'pretty-text',
            'label' => 'Pretty text',
        ]);

        register_block_style('core/post-title', [
            'name' => 'balance-text',
            'label' => 'Balance text',
        ]);

        register_block_style('core/post-title', [
            'name' => 'pretty-text',
            'label' => 'Pretty text',
        ]);

        return $this;
    }
}
