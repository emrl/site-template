import { InspectorControls } from '@wordpress/block-editor'
import { PanelBody, ToggleControl } from '@wordpress/components'
import { createHigherOrderComponent } from '@wordpress/compose'
import { addFilter } from '@wordpress/hooks'

function withSettings(settings, name) {
    if (name === 'core/query') {
        settings.attributes = {
            ...settings.attributes,
            orderByRandom: {
                type: 'boolean',
                default: false,
            },
        }
    }

    return settings
}

const withInspectorControl = createHigherOrderComponent((BlockEdit) => (props) => {
    if (props.name !== 'core/query') {
        return <BlockEdit { ...props }/>
    }

    const { attributes, setAttributes } = props
    const { query, orderByRandom } = attributes

    const setQuery = (random) => {
        setAttributes({
            orderByRandom: random,
            query: {
                ...query,
                orderBy: random ? 'rand' : query.orderBy,
            },
        })
    }

    return (
        <>
            <BlockEdit { ...props } />
            <InspectorControls>
                <PanelBody title="Custom order">
                    <ToggleControl
                      label="Random Order"
                      help="Override the order to be random"
                      checked={ orderByRandom }
                      onChange={ () => setQuery(!orderByRandom) }/>
                </PanelBody>
            </InspectorControls>
        </>
    )
}, 'withInspectorControl')

function extraProps(props, blockType, attributes) {
    if (blockType.name !== 'core/query') {
        return props
    }

    const { orderByRandom = false } = attributes

    return { ...props, orderByRandom }
}

addFilter(
    'blocks.registerBlockType',
    'site/query/withSettings',
    withSettings,
)

addFilter(
    'editor.BlockEdit',
    'site/query/withInspectorControls',
    withInspectorControl,
)

addFilter(
    'blocks.getSaveContent.extraProps',
    'site/query/extraProps',
    extraProps,
)
