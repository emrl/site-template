import { InspectorControls } from '@wordpress/block-editor'
import { PanelBody, PanelRow, ToggleControl } from '@wordpress/components'
import { createHigherOrderComponent } from '@wordpress/compose'
import { addFilter } from '@wordpress/hooks'
import TokenList from '@wordpress/token-list'
import classNames from 'classnames/dedupe.js'

const CLASSNAME = 'is-style-pill'

const withInspectorControls = createHigherOrderComponent((BlockEdit) => (props) => {
    if (props.name !== 'core/button') {
        return <BlockEdit { ...props }/>
    }

    const { attributes, setAttributes } = props
    const { className = '' } = attributes

    const isActive = new TokenList(className).contains(CLASSNAME)

    const setPillStyle = (active) => {
        setAttributes({
            className: classNames(className, {
                [CLASSNAME]: active,
            }),
        })
    }

    return (
        <>
            <InspectorControls>
                <PanelBody>
                    <PanelRow>
                        <ToggleControl
                            label="Enable pill style"
                            help="Adds fully rounded borders and extra inline padding"
                            checked={ isActive }
                            onChange={ () => setPillStyle(!isActive) }
                        />
                    </PanelRow>
                </PanelBody>
            </InspectorControls>
            <BlockEdit { ...props } />
        </>
    )
}, 'withInspectorControls')

addFilter(
    'editor.BlockEdit',
    'site/button/withInspectorControls',
    withInspectorControls,
)
