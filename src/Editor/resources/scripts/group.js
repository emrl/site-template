import { addFilter } from '@wordpress/hooks'

function withSettings(settings, name) {
    if (name === 'core/group') {
        settings.supports = {
            ...settings.supports,
            // Add left, center, and right options
            align: ['wide', 'full', 'left', 'center', 'right'],
        }
    }

    return settings
}

addFilter(
    'blocks.registerBlockType',
    'site/group/withSettings',
    withSettings,
)
