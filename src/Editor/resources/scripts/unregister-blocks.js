import { unregisterBlockType } from '@wordpress/blocks'

window.addEventListener('load', () => {
    unregisterBlockType('core/details')
})
