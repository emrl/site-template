<?php

declare(strict_types=1);

namespace Site\Admin;

use Site\Core\Resource;

class Admin
{
    public function __construct(
        protected readonly Resource $resource,
    ) {
    }

    public function register(): self
    {
        add_action('admin_enqueue_scripts', [$this, 'scripts']);

        return $this;
    }

    public function scripts(): void
    {
        wp_enqueue_style('site-admin', $this->resource->url('c/dist/admin.css'));
    }
}
