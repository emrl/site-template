<?php

declare(strict_types=1);

namespace Site\Admin;

use Site\Integration\AdvancedCustomFields\OptionsPage;

class SiteOptions extends OptionsPage
{
    protected const SLUG = 'site-options';

    protected function page(): array
    {
        return [
            'page_title' => 'Site Options',
            'menu_title' => 'Site Options',
            'menu_slug' => static::SLUG,
            'capability' => 'edit_posts',
            'redirect' => false,
        ];
    }

    protected function group(): array
    {
        return [
            'key' => 'group_site_options',
            'title' => 'Site Options',
            'style' => 'seamless',
            'location' => [
                [
                    [
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => static::SLUG,
                    ],
                ],
            ],
            'fields' => [
                ...apply_filters('site/options/fields', []),
            ],
        ];
    }
}
