<?php

declare(strict_types=1);

namespace Site\Admin;

use Site\Core\Resource;

class Login
{
    public function __construct(
        protected readonly Resource $resource,
    ) {
    }

    public function register(): self
    {
        remove_action('template_redirect', 'wp_redirect_admin_locations', 1000);

        add_action('login_head', [$this, 'logo']);

        add_filter('login_headerurl', fn (): string => home_url());
        add_filter('login_headertext', fn (): string => get_bloginfo('name'));

        return $this;
    }

    public function logo(): void
    {
        $path = 'c/dist/login.svg';

        if (!is_file($this->resource->path($path))) {
            return;
        }

        echo "
            <style>
                #login h1 a {
                    width: 100%;
                    background-image: url({$this->resource->url($path)});
                    background-size: contain;
                    background-position: center;
                }
            </style>
        ";
    }
}
