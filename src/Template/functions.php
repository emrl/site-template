<?php

declare(strict_types=1);

namespace Site\Template;

use Fire\Query\Iterator;
use WP_Query;

use function Fire\Template\buffer;

/**
 * Buffer output from a single post.
 */
function buffer_post(int $id, callable $fn): string
{
    foreach (
        new Iterator(new WP_Query([
            'post_type' => get_post_types(),
            'p' => $id,
        ])) as $p
    ) {
        return buffer($fn);
    }
}
