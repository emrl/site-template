<?php

declare(strict_types=1);

namespace Site\Block;

use Fire\Post\Type;
use stdClass;
use WP_Post;
use WP_Query;

use function Site\Template\buffer_post;

class Block extends Type
{
    public const TYPE = 'wp_block';

    public function register(): self
    {
        add_action('admin_menu', [$this, 'adminMenu']);

        add_filter('walker_nav_menu_start_el', [$this, 'menu'], 10, 4);

        return $this
            ->mergeType([
                'show_in_menu' => true,
                'show_in_nav_menus' => true,
                'menu_icon' => 'dashicons-block-default',
                'supports' => ['revisions'],
            ])
            ->modifyAdminQuery([$this, 'defaultOrder']);
    }

    public function adminMenu(): void
    {
        add_menu_page(
            'Blocks',
            'Blocks',
            'edit_posts',
            'edit.php?post_type=wp_block',
            '',
            'dashicons-block-default',
            20,
        );

        add_submenu_page(
            'edit.php?post_type=wp_block',
            'All Blocks',
            'All Blocks',
            'edit_posts',
            'edit.php?post_type=wp_block',
        );

        add_submenu_page(
            'edit.php?post_type=wp_block',
            'Add New',
            'Add New',
            'publish_posts',
            'post-new.php?post_type=wp_block',
        );
    }

    /**
     * Replace default menu link with the actual block contents.
     */
    public function menu(string $output, WP_Post $item, int $depth, stdClass $args): string
    {
        if ($item->object === static::TYPE) {
            $output = implode('', [
                $args->before,
                buffer_post((int) $item->object_id, 'the_content'),
                $args->after,
            ]);
        }

        return $output;
    }

    public function defaultOrder(WP_Query $query): void
    {
        if (!$query->get('orderby')) {
            $query->set('orderby', ['title' => 'asc']);
        }
    }
}
