<?php

declare(strict_types=1);

namespace Site\Block;

abstract class Type
{
    protected string $path = '';

    public function register(): self
    {
        add_action('init', [$this, 'registerBlock']);

        return $this;
    }

    public function registerBlock(): void
    {
        register_block_type($this->path(), $this->args());
    }

    protected function path(): string
    {
        return $this->path;
    }

    protected function args(): array
    {
        return [];
    }
}
