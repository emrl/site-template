<?php

declare(strict_types=1);

namespace Site\Core;

use PHPMailer\PHPMailer\PHPMailer;

class Mail
{
    public function register(): self
    {
        add_action('phpmailer_init', [$this, 'setupMailer']);

        return $this;
    }

    public function setupMailer(PHPMailer $mailer): void
    {
        if (!$mailer->Sender) {
            $mailer->Sender = $mailer->From;
        }

        if ($host = constant_value('SMTP_HOST')) {
            $mailer->isSMTP();
            $mailer->Host = $host;

            if ($username = constant_value('SMTP_USERNAME')) {
                $mailer->Username = $username;
            }

            if ($password = constant_value('SMTP_PASSWORD')) {
                $mailer->Password = $password;
            }

            if ($port = constant_value('SMTP_PORT')) {
                $mailer->Port = $port;
            }

            if ($secure = constant_value('SMTP_SECURE')) {
                $mailer->SMTPSecure = $secure;
            }

            if ($auth = constant_value('SMTP_AUTH')) {
                $mailer->SMTPAuth = $auth;
            }
        }
    }
}
