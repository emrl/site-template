<?php

declare(strict_types=1);

namespace Site\Core;

class GoogleApiKey
{
    public function __construct(
        protected readonly string $key,
    ) {
    }

    public function register(): self
    {
        if ($this->key) {
            add_filter('acf/settings/google_api_key', fn (): string => $this->key);
            add_filter('aiwp_maps_api_key', fn (): string => $this->key);
        }

        return $this;
    }
}
