<?php

declare(strict_types=1);

namespace Site\Core;

class Headers
{
    public function register(): self
    {
        add_action('send_headers', [$this, 'headers']);

        return $this;
    }

    public function headers(): void
    {
    }
}
