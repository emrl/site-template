<?php

declare(strict_types=1);

namespace Site\Core;

use Fire\Path\JoinPath;

class Resource
{
    public function __construct(
        protected readonly JoinPath $path,
        protected readonly JoinPath $url,
    ) {
    }

    public function path(string ...$paths): string
    {
        return $this->path->path(...$paths);
    }

    public function url(string ...$paths): string
    {
        return $this->url->path(...$paths);
    }
}
