<?php

declare(strict_types=1);

namespace Site\Core;

use Site\Integration\AdvancedCustomFields\FieldGroup;
use stdClass;
use WP_Post;

class NavMenu extends FieldGroup
{
    public function register(): self
    {
        add_filter('nav_menu_css_class', [$this, 'itemClass'], 10, 4);
        add_filter('nav_menu_link_attributes', [$this, 'linkAttributes'], 10, 4);
        add_filter('nav_menu_submenu_css_class', [$this, 'submenuClass'], 10, 3);

        return parent::register();
    }

    protected function group(): array
    {
        return [
            'key' => 'group_nav_menu',
            'title' => 'Nav Menu',
            'location' => [
                [
                    [
                        'param' => 'nav_menu_item',
                        'operator' => '==',
                        'value' => 'all',
                    ],
                ],
            ],
            'fields' => [
                [
                    'key' => 'field_nav_menu_anchor_class',
                    'name' => 'anchor-class',
                    'type' => 'text',
                    'label' => 'Anchor Classes (optional)',
                ],
            ],
        ];
    }

    public function itemClass(array $classes, WP_Post $item, stdClass $args, int $depth): array
    {
        // Dropdown
        if (isset($args->dropdown) && $depth === 0 && in_array('menu-item-has-children', $classes, true)) {
            $classes[] = 'dropdown';
        }

        return $classes;
    }

    public function linkAttributes(array $attrs, WP_Post $item, stdClass $args, int $depth): array
    {
        // Top level
        if (isset($args->dropdown) && $depth === 0 && in_array('menu-item-has-children', $item->classes, true)) {
            $attrs['data-toggle'] = 'dropdown';
        }

        // Sub levels
        if (isset($args->dropdown) && $depth > 0) {
            $attrs['class'] = ($attrs['class'] ?? '').' dropdown-item';
        }

        // Anchor links
        if ($class = get_field('anchor-class', $item->ID)) {
            $attrs['class'] = ($attrs['class'] ?? '').' '.$class;
        }

        return $attrs;
    }

    public function submenuClass(array $classes, stdClass $args, int $depth): array
    {
        if (isset($args->dropdown) && $depth === 0) {
            $classes[] = $args->dropdown;
        }

        return $classes;
    }
}
