<?php

declare(strict_types=1);

namespace Site\Core;

class Request
{
    public function register(): self
    {
        add_action('request', [$this, 'fixEmptySearch']);

        return $this;
    }

    public function fixEmptySearch(array $args): array
    {
        if (empty($args['s'])) {
            unset($args['s']);
        }

        return $args;
    }
}
