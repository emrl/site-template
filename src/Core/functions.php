<?php

declare(strict_types=1);

namespace Site\Core;

use Error;

use function Fire\Core\value;

/**
 * Return environment variable.
 */
function env(string $key, mixed $default = null): mixed
{
    return $_SERVER[$key] ?? $_ENV[$key] ?? value($default);
}

/**
 * Test if app environment matches.
 */
function is_env(string ...$envs): bool
{
    return in_array(env('WP_ENVIRONMENT_TYPE', 'production'), $envs, true);
}

/**
 * Return a constant value if defined and not empty, or default value otherwise.
 */
function constant_value(string $key, mixed $default = null): mixed
{
    try {
        $value = constant($key);

        if (empty($value) && $value !== '' && $value !== 0) {
            return $default;
        }

        return $value;
    } catch (Error) {
        return $default;
    }
}

/**
 * Return URL to webpack dev server if running.
 */
function devServer(string $url): string
{
    if (
        is_env('local', 'development')
        && is_file(HOT_FILE)
        && ($server = file_get_contents(HOT_FILE))
    ) {
        $url = $server;
    }

    return $url;
}
