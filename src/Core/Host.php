<?php

declare(strict_types=1);

namespace Site\Core;

use Stringable;

class Host implements Stringable
{
    public function detectProxy(): self
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
            $_SERVER['HTTPS'] = 'on';
        }

        if (isset($_SERVER['HTTP_X_FORWARDED_HOST'])) {
            $_SERVER['HTTP_HOST'] = $_SERVER['HTTP_X_FORWARDED_HOST'];
        }

        return $this;
    }

    public function host(): string
    {
        return rtrim($_SERVER['HTTP_HOST'] ?? gethostname(), '/');
    }

    public function protocol(): string
    {
        return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? 'https://' : 'http://';
    }

    public function toString(): string
    {
        return $this->protocol().$this->host();
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
