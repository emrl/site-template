<?php

declare(strict_types=1);

namespace Site\Core;

class HashedUploadDirectory
{
    public function register(): self
    {
        add_filter('upload_dir', [$this, 'directory']);

        return $this;
    }

    public function directory(array $data): array
    {
        [$year, $month] = explode('-', current_time('mysql'));
        $hash = substr(sha1($year.$month), 0, 4);

        $data['path'] = $data['basedir']."/$hash";
        $data['url'] = $data['baseurl']."/$hash";
        $data['subdir'] = "/$hash";

        return $data;
    }
}
