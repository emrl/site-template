<?php

declare(strict_types=1);

namespace Site\Core;

use Dotenv\Dotenv;

class Environment
{
    protected array $required = [
        'DB_DATABASE',
        'DB_USERNAME',
        'DB_PASSWORD',
        'AUTH_KEY',
        'SECURE_AUTH_KEY',
        'LOGGED_IN_KEY',
        'NONCE_KEY',
        'AUTH_SALT',
        'SECURE_AUTH_SALT',
        'LOGGED_IN_SALT',
        'NONCE_SALT',
    ];

    public function __construct(
        protected Dotenv $dotenv,
    ) {
    }

    public function load(): self
    {
        if (!isset($_SERVER['WP_ENVIRONMENT_TYPE'])) {
            $this->dotenv->load();
        }

        $this->dotenv
            ->required($this->required)
            ->notEmpty();

        return $this;
    }

    public function required(string ...$keys): self
    {
        $this->required = array_merge($this->required, $keys);

        return $this;
    }
}
